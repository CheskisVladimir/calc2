#include "stdafx.h"
#include "CppUnitTest.h"
#include "TestedClass.hpp"
#include "BracketCheker.hpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTests
{		
	TEST_CLASS(BracketsUnitTest)
	{
	public:
		
		TEST_METHOD(CheckBrackets)
		{
			BracketCheker checker;
			checker.addBracket('(', ')');
			checker.addBracket('{', '}');
			checker.addBracket('[', ']');

			Assert::IsTrue(checker.checkString("()"));
			Assert::IsFalse(checker.checkString(")"));
			Assert::IsFalse(checker.checkString("("));
		}

	};
}