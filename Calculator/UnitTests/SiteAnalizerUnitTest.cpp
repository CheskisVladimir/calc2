#include "stdafx.h"
#include <vector>
#include "CppUnitTest.h"
#include "TestedClass.hpp"
#include "SiteAnalizer.hpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTests
{		
	TEST_CLASS(SiteAnalizerUnitTest)
	{
	public:

		template <class T>
		void compareVectors(const std::vector<T>& expected,
							const std::vector<T>& actual)
		{
			Assert::AreEqual(expected.size(), actual.size());
			for (size_t i = 0; i < actual.size(); i++)
			{
				Assert::AreEqual(expected[i], actual[i]);
			}

			//Assert::AreEqual(expected, actual);
		}
		
		TEST_METHOD(CheckAnalizer)
		{
			SiteAnalizer analizer;
			analizer.visit("1.com");
			analizer.visit("2.com");
			analizer.visit("3.com");
			analizer.visit("4.com");
			analizer.visit("5.com");
			analizer.visit("6.com");
			analizer.visit("1.com");
			analizer.visit("1.com");
			analizer.visit("1.com");
			analizer.visit("2.com");
			analizer.visit("2.com");

			auto top = analizer.getTop();
			std::vector<std::string> expected = { "1.com", "2.com", "6.com", "5.com", "4.com" };
			compareVectors(expected, top);
		}

	};
}