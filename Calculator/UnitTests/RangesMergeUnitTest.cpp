#include "stdafx.h"
#include "CppUnitTest.h"
#include "TestedClass.hpp"
#include "RangeMerger.hpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTests
{		
	TEST_CLASS(RangerMergeUnitTest)
	{
	public:
		
		TEST_METHOD(CheckOperations)
		{
			RangeMerger merger;

			Assert::IsTrue(merger.areMergeble(Range(1.,2.),Range(1.5, 3.)));
			Assert::IsTrue(merger.areMergeble(Range(1., 2.), Range(1.5, 1.8)));
			Assert::IsTrue(merger.areMergeble(Range(1., 2.), Range(-1, 1.2)));

			Assert::IsFalse(merger.areMergeble(Range(1., 2.), Range(2.5, 3.)));
			Assert::IsFalse(merger.areMergeble(Range(-1., 0.), Range(2.5, 3.)));

			{
				Range r(1., 2.);
				merger.addRange(r, Range(1.5, 3));
				Assert::AreEqual(1., r.start);
				Assert::AreEqual(3., r.end);
			}
			{
				Range r(1., 2.);
				merger.addRange(r, Range(1.5, 1.8));
				Assert::AreEqual(1., r.start);
				Assert::AreEqual(2., r.end);
			}
		}
		TEST_METHOD(CheckMerge)
		{
			RangeMerger merger;
			{
				std::vector<Range> src = {
					{1., 2.},
					{1.8, 3.}
				};

				std::vector<Range> dst;
				merger.merge(src, dst);
				Assert::AreEqual(1u, dst.size());
				Assert::AreEqual(1., dst[0].start);
				Assert::AreEqual(3., dst[0].end);
			}
		}
		TEST_METHOD(CheckException)
		{
			RangeMerger merger;
			std::vector<Range> src = {
				{ 1., 2. },
				{ 1.8, -3. }
			};
			std::vector<Range> dst;

			auto f1 = [&] { merger.merge(src, dst); };
			Assert::ExpectException<RangeMergeException>(f1);
		}
	};
}