#include "stdafx.h"
#include "CppUnitTest.h"
#include "TestedClass.hpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTests
{		
	TEST_CLASS(UnitTest1)
	{
		TestedClass tc;
	public:
		
		TEST_METHOD(TestMethod1)
		{
			Assert::AreEqual(4., tc.mult(2, 2));
			//Assert::AreEqual(5., tc.mult(2, 2));
		}

	};
}