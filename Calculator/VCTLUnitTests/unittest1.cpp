#include "stdafx.h"
#include "CppUnitTest.h"
#include "HashMap.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace vctl;

namespace VCTLUnitTests
{		
	TEST_CLASS(HashMapUnitTest)
	{
	public:
		
		TEST_METHOD(SimpleTest)
		{
			hash_map<int, int> map;

			map[1] = 5;
			Assert::AreEqual(5, map[1]);

			hash_map<std::string, int> smap;
			smap["aa"] = 5;
			Assert::AreEqual(5, smap["aa"]);
		}

	};
}