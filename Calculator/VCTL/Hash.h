#pragma once

#include <string>

namespace vctl
{
	template <typename T>
	class hash {};

	template <>
	class hash<int> {
	public:
		size_t operator()(const int &key) const
		{
			return static_cast<size_t>(key);
		}
	};

	template <>
	class hash<std::string> {
	public:
		size_t operator()(const std::string &key) const
		{
			size_t result = 0;
			const int MULTIPLAYER = 31;
			for (auto ch : key)
			{
				result = result * MULTIPLAYER + ch;
			}
			return result;
		}
	};

}