#pragma once

#include "Hash.h"
#include "vector"

namespace vctl
{
	template<typename Key, typename Value, typename Hash = vctl::hash<Key>>
	class hash_map
	{
	public:
		hash_map(size_t tableSize = 32) : m_buckets(tableSize) {}
		hash_map(size_t tableSize, Hash hash) 
			: m_buckets(tableSize), m_hash(hash) {}

		Value& operator[](const Key& key);

	private:

		size_t getBucketIndex(const Key& key) const;

		struct ChainNode
		{
			Key key;
			Value value;
			ChainNode* next = nullptr;
			ChainNode(const Key& _key,
					  ChainNode* _next): key(_key), next(_next){}
			~ChainNode()
			{
				delete next;
			}
		};

		struct Bucket
		{
			ChainNode *node = nullptr;
			~Bucket() { delete node; }
		};

		std::vector<Bucket> m_buckets;
		Hash				m_hash;
	};

	template<typename Key, typename Value, typename Hash>
	Value & hash_map<Key, Value, Hash>::operator[](const Key & key)
	{
		size_t index = getBucketIndex(key);
		for (ChainNode *node = m_buckets[index].node; 
			node != nullptr;
			node = node->next)
		{
			if (node->key == key)
				return node->value;
		}
		m_buckets[index].node = new ChainNode(key, m_buckets[index].node);
		return m_buckets[index].node->value;
	}

	template<typename Key, typename Value, typename Hash>
	inline size_t hash_map<Key, Value, Hash>::getBucketIndex(const Key & key) const
	{
		size_t hash = m_hash(key);
		return hash % m_buckets.size();
	}

}