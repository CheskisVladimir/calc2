#pragma once

#include "IRangeMerger.h"

class RangeMerger : public IRangeMerger
{
public:
	RangeMerger();
	~RangeMerger();

	void merge(const std::vector<Range>& src, std::vector<Range>& dst) override;
public:
	bool areMergeble(const Range& r1, const Range& r2) const;
	void addRange(Range& sum, const Range& added) const;

private:
	//throws if error found RangeMergeException
	void check(const std::vector<Range>& src);

	void bruteForceMerge(const std::vector<Range>& src, std::vector<Range>& dst) const;
};

