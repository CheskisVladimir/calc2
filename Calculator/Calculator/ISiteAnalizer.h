#pragma once

#include <string>
#include <map>
#include <unordered_map>

//Analizes sites popularity, returns the best
struct ISiteAnalizer
{
public: 
	virtual ~ISiteAnalizer(){}
	
	//Increments the site's popularity
	virtual void visit(const std::string& url) = 0;
	//Returns count most popular sites .
	virtual std::vector<std::string> getTop() const = 0;
};