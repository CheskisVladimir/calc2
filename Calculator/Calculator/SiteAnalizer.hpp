#pragma once
#include "ISiteAnalizer.h"

class SiteAnalizer : public ISiteAnalizer
{
public:
	static const size_t TOP_COUNT_DEF = 5;
	SiteAnalizer(size_t topItemsCount = TOP_COUNT_DEF);

	void visit(const std::string& url) override;
	std::vector<std::string> getTop() const override;

private:

	void refreshTopAfterVisit(const std::string& url, size_t count);
	void refreshItemPlace(size_t i);


	//Site visit counts 
	std::map<std::string, int> visitCounts;

	//Information about the URL visiting, stored in the top
	struct TopItem
	{
		std::string url;
		size_t count;
	};
	//sorted array
	std::vector<TopItem>   top;
	size_t topCount;
};

