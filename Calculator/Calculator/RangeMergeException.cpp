#include <sstream>
#include "IRangeMerger.h"

std::string makeMessage(const Range& range)
{
	std::ostringstream stringStream;
	stringStream << "Invalid interval " << ": (" << range.start << ", " << range.end << ")";
	return stringStream.str();
}

RangeMergeException::RangeMergeException(const Range& range)
	: std::runtime_error(makeMessage(range))
{
}