#pragma once

#include <stdexcept>
#include <vector>

struct Range
{
	double start = 0;
	double end = 0;

	Range(double s, double e): start(s), end(e) {}
};

class RangeMergeException : public std::runtime_error
{
public:
	RangeMergeException(const Range& range);
};

struct IRangeMerger
{
public:
	virtual ~IRangeMerger(){}
	virtual void merge(const std::vector<Range>& src, std::vector<Range>& dst) = 0;
};