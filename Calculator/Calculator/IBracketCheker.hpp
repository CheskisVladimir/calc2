#pragma once

#include <memory>
#include <string>

struct IBracketCheker
{
public:
	virtual ~IBracketCheker() {}
	virtual void addBracket(char left, char right) = 0;
	virtual bool checkString(const std::string& str) const = 0;
};