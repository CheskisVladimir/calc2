#pragma once

#include "IBracketCheker.hpp"
#include <vector>
#include <unordered_map>

class BracketCheker : public IBracketCheker
{
public:
	BracketCheker();

	void addBracket(char left, char right) override;
	bool checkString(const std::string& str) const override;
private:

	int bracketsCount;
	std::unordered_map<char, int> leftToCountIndex;
	std::unordered_map<char, int> rightToCountIndex;

	int getIndex(char ch, const std::unordered_map<char, int>& map) const;
	int leftIndex(char ch) const {return getIndex(ch, leftToCountIndex);}
	int rightIndex(char ch) const { return getIndex(ch, rightToCountIndex);}

	bool checkChar(char ch, std::vector<int>& counts) const;
	bool checkAllClosed(const std::vector<int>& counts) const;
};

