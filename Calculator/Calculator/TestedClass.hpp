#pragma once
class TestedClass
{
public:
	TestedClass();
	virtual ~TestedClass();

	double mult(double a, double b) const;
};

