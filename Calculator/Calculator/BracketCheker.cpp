#include "BracketCheker.hpp"



BracketCheker::BracketCheker():
	bracketsCount(0)
{
}

void BracketCheker::addBracket(char left, char right)
{
	leftToCountIndex[left] = bracketsCount;
	rightToCountIndex[right] = bracketsCount;
	++bracketsCount;
}

bool BracketCheker::checkString(const std::string& str) const {
	size_t length = str.length();
	if (bracketsCount == 0 || length == 0)
		return true;
	std::vector<int> counts(bracketsCount, 0);
	for (size_t i = 0; i < length; i++)
	{
		if (!checkChar(str[i], counts))
		{
			return false;
		}
	}
	return checkAllClosed(counts);
}

bool BracketCheker::checkAllClosed(const std::vector<int>& counts) const
{
	for (size_t i = 0; i < counts.size(); i++)
	{
		if (counts[i] != 0)
			return false;
	}
	return true;
}

bool BracketCheker::checkChar(char ch, std::vector<int>& counts) const
{
	int li = leftIndex(ch);
	if (li != -1)
	{
		++counts[li];
		return true;
	}

	int ri = rightIndex(ch);
	if (ri == -1)
		return true;
	if (counts[ri] == 0)
		return false;

	--counts[ri];
	return true;
}

int BracketCheker::getIndex(char ch, const std::unordered_map<char, int>& map) const
{
	auto it = map.find(ch);
	return it == map.end() ? -1 : it->second;
}
