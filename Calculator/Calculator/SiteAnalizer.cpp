#include "SiteAnalizer.hpp"



SiteAnalizer::SiteAnalizer(size_t topItemsCount) : topCount(topItemsCount)
{

}

void SiteAnalizer::visit(const std::string& url)
{
	size_t count = ++visitCounts[url];
	refreshTopAfterVisit(url, count);
}

void SiteAnalizer::refreshTopAfterVisit(const std::string& url, size_t count)
{
	if (top.empty())
	{
		if (topCount > 0)
			top.push_back({ url, count });
		return;
	}

	int itemsPosition = -1;
	//TODO binary search?
	for (size_t i = 0; i < top.size(); i++) 
	{
		if (top[i].url == url) 
		{
			top[i].count = count;
			refreshItemPlace(i);
			return;
		}

		if (top[i].count <= count && itemsPosition == -1)
		{
			itemsPosition = i;
		}
	}

	if (itemsPosition == -1) // The count is less then the top minimum
	{
		if (top.size() < topCount)
			top.push_back({ url, count });
		return;
	}
	top.insert(top.begin() + itemsPosition, { url, count });
	if (top.size() > topCount)
		top.pop_back();
}

void SiteAnalizer::refreshItemPlace(size_t pos)
{
	//TODO Binary search
	for (int i = int(pos); i > 0; i--)
	{
		if (top[i].count >= top[i - 1].count)
		{
			std::swap(top[i], top[i - 1]);
		}
		else
		{
			break;
		}
	}
}

std::vector<std::string> SiteAnalizer::getTop() const
{
	std::vector<std::string> result;
	for (const auto& item : top)
	{
		result.push_back(item.url);
	}
	return result;
}

