#include <algorithm>
#include "RangeMerger.hpp"



RangeMerger::RangeMerger()
{
}


RangeMerger::~RangeMerger()
{
}

void RangeMerger::merge(const std::vector<Range>& src, std::vector<Range>& dst)
{
	check(src);
	bruteForceMerge(src, dst);

}

void RangeMerger::check(const std::vector<Range>& src)
{
	for (const auto& range : src)
	{
		if (range.end < range.start)
		{
			throw RangeMergeException(range);
		}
	}
}

void RangeMerger::addRange(Range& sum, const Range& added) const
{
	sum.start = std::min(sum.start, added.start);
	sum.end = std::max(sum.end, added.end);
}

bool RangeMerger::areMergeble(const Range& r1, const Range& r2) const
{
	if (r2.start > r1.end)
		return false;
	if (r2.end < r1.start)
		return false;
	return true;
}

void RangeMerger::bruteForceMerge(const std::vector<Range>& src, std::vector<Range>& dst) const
{
	dst = src;
	bool somethingMerged = true;
	while (somethingMerged)
	{
		somethingMerged = false;
		for (auto i = 0; i < dst.size(); i++)
		{
			for (auto j = int(dst.size()) - 1 ; j > i; j--)
			{
				if (areMergeble(dst[i], dst[j]))
				{
					somethingMerged = true;
					addRange(dst[i], dst[j]);
					dst.erase(dst.begin() + j);
				}
			}
		}
	}
}
